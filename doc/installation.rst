.. _installation:
.. index:: Installation

Installation
************

Note that :program:`vcsgc-lammps` first requires you to compile
:program:`LAMMPS` from source. Please consult the `LAMMPS homepage
<http://lammps.sandia.gov/>`_ for instructions.

You can retrieve the most recent version of :program:`vcsgc-lammps` by
executing the following commands in the LAMMPS ``src/`` directory::

  wget https://vcsgc-lammps.materialsmodeling.org/vcsgc-lammps.tgz
  tar xfz vcsgc-lammps.tgz

This will extract the source code of the module to the ``USER-VCSGC/`` subdirectory.
Now activate the module by running::

  make yes-user-vcsgc

Finally, recompile LAMMPS as usual with the command ``make <machine-name>``.
After the compilation has completed, you can test the installation
by running the included example LAMMPS script::

  cd USER-VCSGC/examples/
  ../../lmp -in example_input_sgcmc.in

Here, ``lmp`` should be replaced with the path to your :program:`LAMMPS`
executable.

**Note to users of the 'eam/cd' pair style:**
If you plan to use the MC routine with the concentration-dependent EAM model
(`pair_style eam/cd <http://lammps.sandia.gov/doc/pair_eam.html>`_), you also have to install this pair
style by running ``make yes-user-misc``. Then the preprocessor definition
``CDEAM_MC_SUPPORT`` at the top of the source file ``fix_semigrandcanonical_mc.h`` has to be set to 1. 
